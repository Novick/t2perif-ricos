/*
Serial Echo Server
Cada byte recebido e reenvidado pela serial.
*/

const int ledPin1 = 9;
const int ledPin2 = 10;
const int ledPin3 = 12;

int ledPin1State = 0;
int ledPin2State = 0;
int ledPin3State = 0;

uint8_t countBtn1;
uint8_t countBtn2;
uint8_t countBtn3;

boolean btn1Flag = false;
boolean btn2Flag = false;
boolean btn3Flag = false;

int btn1 = 3;
int btn2 = 4;
int btn3 = 5;

int btnState1;
int btnState2;
int btnState3;

char inByte;

void setup() {  
  
  /* Inicializa a porta serial em 9600 bytes/s */
  Serial.begin(9600);
  countBtn1 = 0;
  countBtn2 = 0;
  countBtn3 = 0;

  /* Configura o pino de IO para piscar o led durante a recepção.*/
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);

  pinMode(btn1, INPUT);
  pinMode(btn2, INPUT);
  pinMode(btn3, INPUT);
  
  /* Seta botao */
  digitalWrite(btn1, HIGH);
  digitalWrite(btn2, HIGH);
  digitalWrite(btn3, HIGH);
}

void loop() {

  /* Aguarda o recebimento de dados */
  if (Serial.available() > 0) {

    /* Le byte da serial */
    inByte = Serial.read();
    

    ledPin1State = digitalRead(ledPin1);
    ledPin2State = digitalRead(ledPin2);
    ledPin3State = digitalRead(ledPin3);

    /* Inverte o estado do pino 9 */
    if(inByte == '1'){
       digitalWrite(ledPin1, !ledPin1State);
    }
    /* Inverte o estado do pino 10 */
    else if(inByte == '2'){
      digitalWrite(ledPin2, !ledPin2State);
    }
    
    /* Inverte o estado do pino 12 */
    else if(inByte == '3' ){
      digitalWrite(ledPin3, !ledPin3State);
    }
    
    else if(inByte == 'z'){
      Serial.write(countBtn1);
    }
    
    else if(inByte == 'x'){
      Serial.write(countBtn2);
    }

    else if(inByte == 'c'){
      Serial.write(countBtn3);
    }   
  }

  btnState1 = digitalRead(btn1);
  btnState2 = digitalRead(btn2);
  btnState3 = digitalRead(btn3);

  if(btnState1 == LOW && btn1Flag == false ) {
    btn1Flag=true;
    countBtn1++;
  }
  else if(btnState1 == HIGH){
    btn1Flag = false;
  }
  
  if(btnState2 == LOW && btn2Flag == false ) {
    countBtn2++;
    btn2Flag=true;
  }
  else if(btnState2 == HIGH){
    btn2Flag = false;
  }
  
  if(btnState3 == LOW && btn3Flag == false ) {
    countBtn3++;
    btn3Flag=true;
  }
  else if(btnState3 == HIGH){
    btn3Flag = false;
  }  
  delay(50);
}




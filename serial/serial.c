#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>

#define N_THREADS 2
 

int fd;
 
int config_serial(char * device, unsigned int baudrate){
    struct termios options;
    int fd;
 
    fd = open(device, O_RDWR | O_NOCTTY | O_NDELAY );
    if (fd < 0)
    {
        /*
        * Could not open the port.
        */
 
        perror("config_serial: Não pode abrir a serial - ");
        return -1;
    }
     
    fcntl(fd, F_SETFL, 0);
 
    /*
    * Get the current options for the port...
    */
    tcgetattr(fd, &options);
 
    /* sets the terminal to something like the "raw" mode */
    cfmakeraw(&options);
     
    /*
    * Set the baudrate...
    */
    cfsetispeed(&options, baudrate);
    cfsetospeed(&options, baudrate);
     
    /*
    * Enable the receiver and set local mode...
    */
    options.c_cflag |= (CLOCAL | CREAD);
     
    /*
     * No parit, 1 stop bit, size 8
     */
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;
     
 
    /*
     * Clear old settings
     */
    options.c_cflag &= ~CRTSCTS;
    options.c_iflag &= ~(IXON | IXOFF | IXANY);
     
    /* non-caninical mode */
    options.c_lflag &= ~ICANON;
     
    /*
    * Set the new options for the port...
    */
    tcsetattr(fd, TCSANOW, &options);
     
    /* configura a tty para escritas e leituras não bloqueantes */
    //fcntl(fd, F_SETFL, fcntl(fd, F_GETFL) | O_NONBLOCK);
     
    return fd;
}
 
void* countButtonsPressed(void* arg) {
    uint8_t countBtn1, countBtn2, countBtn3;
    countBtn1 = countBtn2 = countBtn3 = 0;
    
    char buffer[3];
    uint8_t results[3];
    
    results[0] = results[1] = results[2] = 0;
    
    buffer[0] = 'z';
    buffer[1] = 'x';
    buffer[2] = 'c';
     
    while(1) { 
        write(fd, &buffer[0], 1);
	    read(fd, &results[0], 1);
       	
	    write(fd, &buffer[1], 1);
	    read(fd, &results[1], 1);
       	
	    write(fd, &buffer[2], 1);
	    read(fd, &results[2], 1);
	
	    if(countBtn1 <  results[0]) {
            countBtn1 = results[0];
            printf("\nButton 1 pressed %d times", countBtn1);
        }
	
	    if(countBtn2 <  results[1]) {
            countBtn2 = results[1];
            printf("\nButton 2 pressed %d times", countBtn2);
        }
   	 
	    if(countBtn3 <  results[2]) {
            countBtn3 = results[2];
            printf("\nButton 3 pressed %d times", countBtn3);
        }
    }
}
 
void* readFromCmdLine(void* arg) {
    
    while(1) {
        char val;
        
        scanf("%c", &val);
        if(val == '1' || val == '2' || val == '3'){
		   write(fd, &val, 1);	
        } 
    }
    close(fd);                     
}
  
int main(int argc, char** argv){
     
    fd = config_serial("/dev/ttyAMA0", B9600);
     
    if(fd<0){
        return 0;
    }
     
    pthread_t threads[N_THREADS];
    
    printf("Informe os números 1, 2 ou 3 para ascender e apagar os leds!\n");  
    pthread_create(&threads[1], NULL, countButtonsPressed, NULL);  
    pthread_create(&threads[0], NULL, readFromCmdLine, NULL);  
    
    pthread_exit(NULL);
     
    return 0;
}  
